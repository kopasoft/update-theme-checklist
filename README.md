# Update Theme Checklist

## Yêu cầu

- [Git SCM](https://git-scm.com/) hoặc [TortoiseGit](https://tortoisegit.org/download/)
- [XAMPP](https://www.apachefriends.org/index.html) hoặc [MAMP](https://www.mamp.info/en/downloads/) hoặc là [WampServer](http://www.wampserver.com/en/)

## Thiết lập

1. Cài đặt WordPress cho development
2. Define `WP_DEBUG` trong file `wp-config.php` thành `true`
3. Cài đặt theme và những plugin đính kèm với theme

## Kiểm tra lại theme sau khi đã update xong

1. Đã update version trong file `style.css`

## Commit code và release

1. Commit code lên bitbucket
```
git commit -m . "<message>"
// ví dụ: git commit -m . "fix bug giao dien"
git push origin <branch_name>
// ví dụ: git push origin master
```
2. Tạo tag theo version
```
git tag <tag_name> 
// ví dụ: git tag v1.0.0
git push --tags
```